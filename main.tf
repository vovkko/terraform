terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.45.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.26.0"
    }
  }
}

# get variables 
variable "hetzner_token" {
  sensitive = true
}
variable "cloudflare_token" {
  sensitive = true
}
variable "cloudflare_zone_id" {
  sensitive = true
}
variable "cloudflare_subdomain" {
  sensitive = true
}
variable "hetzner_server" {
  sensitive = false
}
variable "hetzner_dc" {
  sensitive = false
}
variable "laravel_path" {
  sensitive = false
}

# generate SSH key
provider "tls" {}

resource "tls_private_key" "ssh_key" {
  algorithm = "ED25519"
}

resource "local_file" "private_key" {
  filename = "${path.module}/ssh/private_key"
  content  = tls_private_key.ssh_key.private_key_pem
}

resource "local_file" "public_key" {
  filename = "${path.module}/ssh/public_key.pub"
  content  = tls_private_key.ssh_key.public_key_openssh
}

# setup providers
provider "cloudflare" {
  api_token = var.cloudflare_token
}

provider "hcloud" {
  token = var.hetzner_token
}

resource "hcloud_ssh_key" "ssh" {
  name       = "ssh"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

# create server
resource "hcloud_server" "node1" {
  name        = "node1"
  image       = "ubuntu-22.04"
  server_type = var.hetzner_server
  datacenter  = var.hetzner_dc
  ssh_keys    = ["ssh"]
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
  user_data  = file("docker.yml")
  depends_on = [hcloud_ssh_key.ssh]
}

# setup dns
resource "cloudflare_record" "dns" {
  zone_id    = var.cloudflare_zone_id
  name       = var.cloudflare_subdomain
  value      = hcloud_server.node1.ipv4_address
  type       = "A"
  ttl        = 3600
  depends_on = [hcloud_server.node1]
}

# sync files
resource "null_resource" "sync_folder" {
  triggers = {
    always_run = timestamp()
  }
  depends_on = [hcloud_server.node1]

  provisioner "local-exec" {
    command = "rsync -avz -e 'ssh -o StrictHostKeyChecking=no' ${var.laravel_path}/ user@${hcloud_server.node1.ipv4_address}:~/app"
  }
}
