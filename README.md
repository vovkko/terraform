# Terraform 

## Description

Creates server on Hetzner Cloud platform, install docker on created server and copy files from local computer.
Also register server ip as subdomain on your cloudflare

## Usage

1. Copy config file `cp terraform.tfvars.example terraform.tfvars`
1. Fill `terraform.tfvars` with required credentials
1. Install dependencies with `terraform init`
1. Run process with `terraform `